<?php

if (!defined('BKW3S_NATURE_VIEW_BASEPATH')) {
    define('BKW3S_NATURE_VIEW_BASEPATH', realpath(__DIR__ . '/view/'));
}

add_theme_support('title-tag');
add_theme_support('post-formats', ['gallery', 'image']);

add_image_size('W3BK_crop_200x200', 200, 200, true);
add_image_size('W3BK_crop_400x400', 400, 400, true);

add_filter('image_size_names_choose', 'bkw3s_custom_sizes');
function bkw3s_custom_sizes($sizes)
{
    return array_merge($sizes, [
        'W3BK_200x200' => __('BKW3SD Crop 200x200'),
        'W3BK_400x400' => __('BKW3SD Crop 400x400'),
    ]);
}

require_once 'inc/customizer/bkw3s_background_colors.php';

/**
 * Apply categories to attachments
 */
add_action('init', 'bkw3s_apply_categories_to_attachments');
function bkw3s_apply_categories_to_attachments()
{
    register_taxonomy_for_object_type('category', 'attachment');
}

/**
 * Apply tags to attachments
 */
add_action('init', 'bkw3s_apply_tags_to_attachments');
function bkw3s_apply_tags_to_attachments()
{
    register_taxonomy_for_object_type('post_tag', 'attachment');
}

/**
 * CSS & JavaScript files
 */
add_action('wp_enqueue_scripts', 'bkw3s_scripts');
function bkw3s_scripts()
{
//	wp_enqueue_style( 'w3bk_css_reset', get_stylesheet_directory_uri() . '/css/reset.css', [], '0.0.1' );
    wp_enqueue_style('w3bk_css_normalize', get_stylesheet_directory_uri() . '/css/normalize.css', [], '0.0.1');
    wp_enqueue_style('w3bk_fontawesome', get_stylesheet_directory_uri() . '/font/fontawesome/css/all.css', [], '5.13.0', 'all');
    wp_enqueue_style('w3bk_w3css', get_stylesheet_directory_uri() . '/css/w3schools.css', false, '0.0.5');
    wp_enqueue_style('w3bk_lightbox2_css', get_stylesheet_directory_uri() . '/lightbox2/css/lightbox.min.css', [], '2.11.1', 'all');
    wp_enqueue_style('w3bk_style', get_stylesheet_directory_uri() . '/style.css', [], '0.0.17', 'all');
    // jquery before jquery user
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/jquery.js', [], '3.5.0');
    wp_enqueue_script('w3bk_trinket_js', get_stylesheet_directory_uri() . '/js/trinket.js', [], '0.0.5');
    wp_enqueue_script('w3bk_slider_js', get_stylesheet_directory_uri() . '/js/slider.js', [], '0.0.2');
    wp_enqueue_script('w3bk_lightbox2_js', get_stylesheet_directory_uri() . '/lightbox2/js/lightbox.min.js', [], '2.11.1');
}

add_action('init', 'bkw3s_register_menus');
function bkw3s_register_menus()
{
    register_nav_menus(
        [
            'header-menu' => __('Header Menu'),
            'site-menu-right' => __('Site Menu Right'),
            'footer-menu' => __('Footer Menu')
        ]
    );
}

/**
 * https://codex.wordpress.org/Function_Reference/register_sidebar
 */
add_action('init', 'bkw3s_register_sidebars');
function bkw3s_register_sidebars()
{
    register_sidebar(
        [
            'name' => __('BKW3SD Sidebar Right', 'wp-bitkorn-w3schools-gas'),
            'id' => 'w3bk-sidebar-right', // ID should be LOWERCASE  ! ! !
            'description' => '',
            'class' => '',
            'before_widget' => '<li id="%1$s" class="%2$s">',
            'after_widget' => '</li>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ]
    );
}

/**
 * custom nav walker.
 */
if (!class_exists('Bkw3s_NavWalker')) {
    require_once(get_template_directory() . '/inc/Bkw3s_NavWalker.php');
}

/**
 * translations
 */
add_action('after_setup_theme', 'bkw3s_localisation');
function bkw3s_localisation()
{
    load_theme_textdomain('wp-bitkorn-w3schools-gas', get_stylesheet_directory() . '/languages');
}

require_once 'inc/bkw3s_cpt_block-part.php';
require_once 'inc/acf/bkw3s_cpt-block-part_acf-fieldgroup.php';
require_once 'inc/bkw3s_cpt_block-manufac.php';
require_once 'inc/acf/bkw3s_cpt-block-manufac_acf-fieldgroup.php';

/**
 * Filter Post Links for my CPT.
 *
 * @param $post_link
 * @param $post
 *
 * @return string|string[]
 */
add_filter('post_type_link', 'bkw3s_show_cpt_permalinks', 1, 2);
function bkw3s_show_cpt_permalinks($post_link, $post)
{
    if (is_object($post) && in_array($post->post_type, ['gas_block_part', 'gas_block_manufac'])) {
        $terms = wp_get_object_terms($post->ID, 'gas_category');
        if (!$terms instanceof WP_Error) {
            return str_replace('%gas_category%', $terms[0]->slug, $post_link);
        } else if ($terms instanceof WP_Error) {
        }
    }

    return $post_link;
}

/**
 * category & tag support for pages (not posts ...posts already has taxonomy)
 * thanks to https://thewphosting.com/add-categories-tags-pages-wordpress/
 */
add_action('init', 'bkw3s_add_taxonomies_to_pages');
function bkw3s_add_taxonomies_to_pages()
{
    register_taxonomy_for_object_type('post_tag', 'page');
    register_taxonomy_for_object_type('category', 'page');
}

/**
 * Make "category & tag support for pages" work.
 *
 * @param $wp_query
 */
if (!is_admin()) {
    add_action('pre_get_posts', 'bkw3s_category_and_tag_archives');
}
function bkw3s_category_and_tag_archives($wp_query)
{
    $my_post_array = ['post', 'page'];

    if ($wp_query->get('category_name') || $wp_query->get('cat')) {
        $wp_query->set('post_type', $my_post_array);
    }

    if ($wp_query->get('tag')) {
        $wp_query->set('post_type', $my_post_array);
    }
}

/**
 * sort order by "Reihenfolge"
 * https://wordpress.stackexchange.com/a/39818/187922
 */
//add_action( 'pre_get_posts', 'bkw3s_change_sort_order' );
function bkw3s_change_sort_order($query)
{
    if (is_archive()):
        //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
        //Set the order ASC or DESC
        $query->set('order', 'ASC');
        //Set the orderby
        $query->set('orderby', 'title');
    endif;
}

/**
 * full wide image over content
 * unused, weil get_the_post_thumbnail() in header.php verwendet wird
 */
$fwImages = [
    88 => '/wp-content/uploads/2020/06/representatives_contact_banner.png',
];
function bkw3s_content_image_full_wide($contentId)
{
    global $fwImages;
    if (empty($contentId) || !key_exists($contentId, $fwImages)) {
        return '';
    }
    return '<img src="' . $fwImages[$contentId] . '" alt="representatives" class="w3-image" style="width: 100%">';
}
