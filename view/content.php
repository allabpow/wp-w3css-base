<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php // post_class(); ?>>
    <div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
    </div>
    <header class="entry-header">
		<?php
		//        if (is_single()) :
		//            the_title('<h1 class="entry-title">', '</h1>');
		//        else :
		//            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '">', '</a></h2>');
		//        endif;
		?>
    </header><!-- .entry-header -->
    <div class="entry-content">
		<?php
		if ( is_single() ) :
			the_content();
		else :
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bitkorn-w3schools-gas' ) );
		endif;

		wp_link_pages( [
			'before' => '<div class="page-links">',
			'after'  => '</div>',
		] );
		?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
		<?php // wp_bootstrap_starter_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
