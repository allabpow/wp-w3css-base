let currentSlideNo = 1;
showSlide(1);

function navSlide(amount) {
    if (amount < 0) {
        showSlide(currentSlideNo - 1);
    } else {
        showSlide(currentSlideNo + 1);
    }
}

function showSlide(slideNo) {
    slideNo = parseInt(slideNo);
    const slideContainerIdString = $('#slide-item-1').closest('.slide-container').attr('id');
    if (!slideContainerIdString) {
        return;
    }
    const slideContainerNo = slideContainerIdString.split('-')[2];
    if (!slideContainerNo) {
        return;
    }
    const slides = $('#slide-container-' + slideContainerNo).find('.slide-item');
    if (slideNo > slides.length) {
        slideNo = 1;
    } else if (slideNo < 1) {
        slideNo = slides.length;
    }
    currentSlideNo = slideNo;
    const slideItem = $('#slide-item-' + slideNo);
    if (!slideItem) {
        showSlide(1);
    }

    slides.each(function (index, element) {
        $(element).css('display', 'none');
    });
    slideItem.css('display', 'block');

    $('.fa-circle').each(function (index, element) {
        if ($(element).hasClass('fas')) {
            $(element).removeClass('fas');
            $(element).addClass('far');
        }
    });
    const circleElem = $('#slide-circle-' + slideNo);
    circleElem.removeClass('far');
    circleElem.addClass('fas');

}

$(document).on('click', '.slide-nav-item', function () {
    const id = $(this).attr('id');
    showSlide(id.split('-')[3]);
});
