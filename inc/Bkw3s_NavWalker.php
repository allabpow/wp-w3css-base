<?php

/*
 * https://codex.wordpress.org/Class_Reference/Walker
 */

if ( ! class_exists( 'Bkw3s_NavWalker' ) ) {

	/**
	 * Description of Bkw3sd_Walker_Nav_Menu
	 * https://developer.wordpress.org/reference/classes/walker_nav_menu/
	 * @author Torsten Brieskorn
	 */
	class Bkw3s_NavWalker extends Walker_Nav_Menu {

		protected $currentDropDownId = '';

		/**
		 * Traverse elements to create list from elements.
		 *
		 * Display one element if the element doesn't have any children otherwise,
		 * display the element and its children. Will only traverse up to the max
		 * depth and no ignore elements under that depth. It is possible to set the
		 * max depth to include all depths, see walk() method.
		 *
		 * This method should not be called directly, use the walk() method instead.
		 *
		 * @param object $element Data object.
		 * @param array $children_elements List of elements to continue traversing (passed by reference).
		 * @param int $max_depth Max depth to traverse.
		 * @param int $depth Depth of current element.
		 * @param array $args An array of arguments.
		 * @param string $output Used to append additional content (passed by reference).
		 *
		 * @since 2.5.0
		 *
		 */
		public function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
			if ( ! $element ) {
				return;
			}

			$id_field = $this->db_fields['id'];
			$id       = $element->$id_field;

			// Display this element.
			$this->has_children = ! empty( $children_elements[ $id ] );
			if ( isset( $args[0] ) && is_array( $args[0] ) ) {
				$args[0]['has_children'] = $this->has_children; // Back-compat.
			}

			$displayChildren = ( 0 == $max_depth || $max_depth > $depth + 1 ) && isset( $children_elements[ $id ] );

			if ( $displayChildren ) {
				// therefore the class is overwritten
				$this->currentDropDownId = 'dropdown_' . ( time() + rand( 1000000, 100000000 ) );
				$output                  .= '<div class="w3-dropdown-click w3-mobile" onclick="dropdownClick(\'' . $this->currentDropDownId . '\')">';
			}

			$this->start_el( $output, $element, $depth, ...array_values( $args ) );

			// Descend only when the depth is right and there are childrens for this element.
			if ( $displayChildren ) {

				foreach ( $children_elements[ $id ] as $child ) {

					if ( ! isset( $newlevel ) ) {
						$newlevel = true;
						// Start the child delimiter.
						$this->start_lvl( $output, $depth, ...array_values( $args ) );
					}
					$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
				}
				unset( $children_elements[ $id ] );
			}

			if ( isset( $newlevel ) && $newlevel ) {
				// End the child delimiter.
				$this->end_lvl( $output, $depth, ...array_values( $args ) );
			}

			// End this element.
			$this->end_el( $output, $element, $depth, ...array_values( $args ) );

			if ( $displayChildren ) {
				// therefore the class is overwritten
				$output .= '</div>';
			}
		}

		/**
		 * Starts the list before the elements are added.
		 *
		 * @param string $output Used to append additional content (passed by reference).
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param stdClass $args not used
		 *
		 * @since 3.0.0
		 *
		 * @see Walker_Nav_Menu::start_lvl()
		 *
		 */
		public function start_lvl( &$output, $depth = 0, $args = [] ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "\n{$indent}<div class=\"w3-dropdown-content w3-bar-block\" id='{$this->currentDropDownId}'>\n<div class=\"w3-dropdown-c-inner\">\n";
		}

		/**
		 * Ends the list of after the elements are added.
		 *
		 * @param string $output Used to append additional content (passed by reference).
		 * @param int $depth Depth of page. Used for padding.
		 * @param stdClass $args Not used.
		 *
		 * @since 3.0.0
		 *
		 * @see Walker_Nav_Menu::end_lvl()
		 *
		 */
		public function end_lvl( &$output, $depth = 0, $args = [] ) {
			$indent = str_repeat( "\t", $depth );
			$output .= "\n$indent</div>\n</div>\n";
		}

		/**
		 * Starts the element output.
		 *
		 * @param string $output Used to append additional content (passed by reference).
		 * @param WP_Post $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param stdClass $args An object of wp_nav_menu() arguments.
		 * @param int $id Current item ID.
		 *
		 * @since 3.0.0
		 *
		 * @global int $_nav_menu_placeholder
		 * @global int|string $nav_menu_selected_id
		 *
		 * @see Walker_Nav_Menu::start_el()
		 *
		 */
		public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
			$atts = [];
			if ( empty( $item->attr_title ) ) {
				$atts['title'] = ! empty( $item->title ) ? strip_tags( $item->title ) : '';
			} else {
				$atts['title'] = $item->attr_title;
			}
			$atts['target'] = ! empty( $item->target ) ? $item->target : '';
			$childs         = isset( $args->walker->has_children ) && $args->walker->has_children;
			$caret          = '';
			if ( $childs ) {
				$atts['class'] = 'w3-button';
				$caret         = ' <i class="fa fa-caret-down"></i>';
			} else {
				$atts['href']  = ! empty( $item->url ) ? $item->url : '#';
				$atts['class'] = 'w3-bar-item w3-button w3-mobile';
			}
			if ( $depth == 0 ) {
				$atts['class'] .= ' bkw3s-menu-item-1';
			} else {
				$atts['class'] .= ' bkw3s-menu-item-inner';
			}

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value      = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}
			$item_output = $args['before'] ?? '';
			$item_output .= ( $childs ? '<button' : '<a' ) . $attributes . '>';
			$item_output .= ( $args['link_before'] ?? '' ) . $item->title . $args['link_after'];
			$item_output .= ( $childs ? $caret . '</button>' : '</a>' );
			$item_output .= $args['after'] ?? '';

			// END appending the internal item contents to the output.
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

		public function end_el( &$output, $item, $depth = 0, $args = [] ) {
			$output .= "\n";
		}

	}

}
