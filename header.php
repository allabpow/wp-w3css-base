<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
//global $logger;
//$obj = get_post_type_object('gas_block_part');
//$logger->info(print_r($obj, true));
//$theLink = get_post_type_archive_link('gas_block_part');
//$logger->info(print_r($theLink, true));
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/img/favicon.ico?v=0.0.1" type="image/x-icon">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page">
    <section class="page-header-topping">
        <div class="w3-row">
            <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
            <div class="w3-col l8 m10">
                <div class="w3-row">
                    <div class="w3-col l2 w3-hide-medium w3-hide-small">&nbsp;</div>
                    <div class="w3-col l8 w3-center" style="padding-top: 6px">
                        <a class="" href="<?= get_home_url() ?>" style="white-space: nowrap;">
                            <img src="<?= get_stylesheet_directory_uri() ?>/img/logo_aircraft.svg" class="w3-image page-header-logo" alt="Aircraft Logo">
                            <br>
                            <strong class="page-header-logo-text w3-block">GOMOLZIG Aircraft Services GmbH</strong>
                        </a>
                    </div>
                    <div class="w3-col l2 w3-hide-medium w3-hide-small">&nbsp;</div>
                </div>
                <div class="w3-row">
                    <div class="w3-col" style="width: 200px; float: right">
                        <!--                <div class="w3-hide-large"><br></div>-->
                        <?php get_search_form() ?>
                    </div>
                </div>
            </div>
            <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
        </div>
    </section>
    <header id="page_header" class="page-header w3-row">
        <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
        <nav class="menu-topmenu-container w3-col l8 m10" id="nav-top">
            <div id="menu-topmenu" class="w3-bar">

                <div class="w3-dropdown-click w3-mobile" onclick="dropdownClick('jumbonav_capabilitis'); adjustJumbo('jumbonav_capabilitis')">
                    <button class="w3-button bkw3s-menu-item-1">Capabilities <i class="fa fa-caret-down"></i></button>
                    <div class="w3-dropdown-content gas-jumbo-content" id="jumbonav_capabilitis">
                        <div class="w3-dropdown-c-inner w3-panel">
                            <div class="gas-jumbo-block">
                                <h4 class="gas-jumbo-h">Engineering</h4>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Introduction</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Type Certificates</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Supplemental Type Certificates</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Quiet Flight - Exhaust Systems</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Mission Equipment</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Power Plant Modification</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Repairs and Changes</a>
                            </div>
                            <div class="gas-jumbo-block">
                                <h4 class="gas-jumbo-h">Products & Special Sales</h4>
                                <a href="/gas-part" class="gas-jumbo">Products</a>
                                <!--                                <a href="--><?php //echo get_category_link('products') ?><!--" class="gas-jumbo">Products</a>-->
                            </div>
                            <div class="gas-jumbo-block">
                                <h4 class="gas-jumbo-h">Part M - CAMO+</h4>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Scope of Service</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> CAMO Portal</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> CAMO FAQ</a>
                            </div>
                            <div class="gas-jumbo-block">
                                <h4 class="gas-jumbo-h">Production</h4>
                                <a href="/gas-manufac/aircraft-components" class="gas-jumbo">Aircraft Components</a>
                                <a href="/gas-manufac/welding" class="gas-jumbo">Welding</a>
                                <a href="/gas-manufac/low-volume-series" class="gas-jumbo">Low Volume Series</a>
                                <a href="/gas-manufac/extended-supply-chain" class="gas-jumbo">Extended Supply Chain</a>
                            </div>
                            <div class="gas-jumbo-block">
                                <h4 class="gas-jumbo-h">
                                    <strong class="w3-large">M</strong>aintenance-<strong class="w3-large">R</strong>epair-<strong class="w3-large">O</strong>verhaul
                                </h4>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Maintenance-Repair-Overhaul</a>
                                <a href="#" class="gas-jumbo"><i class="fas fa-exclamation w3-text-red"></i> Aircraft Maintenance</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                <a class="w3-button w3-bar-item bkw3s-menu-item-1" href="/gas_block_part/lorem-gas-block/">One Part</a>-->
                <!--                <a class="w3-button w3-bar-item bkw3s-menu-item-1" href="--><?php //echo get_post_type_archive_link('gas_block_part') ?><!--">Parts</a>-->
                <?php wp_nav_menu([
                    'theme_location' => 'header-menu',
                    'walker' => new Bkw3s_NavWalker(),
                    'container' => '',
//					'menu_class'     => 'w3-bar',
//					'items_wrap'     => '<div id="%1$s" class="%2$s">%3$s</div>',
                    'items_wrap' => '%3$s',
                ]) ?>

                <!--                <div class="w3-right w3-bar-item bkw3s-menu-item-1">-->
                <!--					--><?php //get_search_form() ?>
                <!--                </div>-->
            </div>
        </nav>
        <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
    </header> <!--  #masthead -->

    <?php if (is_front_page()): ?>
        <div id="slide-container-1" class="w3-display-container slide-container">

            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/slider/business_website_templates_1.jpg"
                 id="slide-item-1" class="slide-item w3-animate-opacity" style="width:100%" alt="slider">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/slider/business_website_templates_2.jpg"
                 id="slide-item-2" class="slide-item w3-animate-opacity" style="width:100%; display: none" alt="slider">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/slider/business_website_templates_3.jpg"
                 id="slide-item-3" class="slide-item w3-animate-opacity" style="width:100%; display: none" alt="slider">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/slider/business_website_templates_4.jpg"
                 id="slide-item-4" class="slide-item w3-animate-opacity" style="width:100%; display: none" alt="slider">

            <div class="w3-center w3-display-middle" style="width: 100%">
                <div class="w3-left gas-brand w3-padding cursor-pointer" onclick="navSlide(-1)">&#10094;</div>
                <div class="w3-right gas-brand w3-padding cursor-pointer" onclick="navSlide(1)">&#10095;</div>
            </div>

            <div class="w3-center w3-display-bottommiddle">
                <span id="slide-nav-item-1" class="cursor-pointer" onclick="showSlide(1)"><i id="slide-circle-1" class="fas fa-circle"></i></span>
                <span id="slide-nav-item-2" class="cursor-pointer" onclick="showSlide(2)"><i id="slide-circle-2" class="far fa-circle"></i></span>
                <span id="slide-nav-item-3" class="cursor-pointer" onclick="showSlide(3)"><i id="slide-circle-3" class="far fa-circle"></i></span>
                <span id="slide-nav-item-4" class="cursor-pointer" onclick="showSlide(4)"><i id="slide-circle-4" class="far fa-circle"></i></span>
            </div>
        </div>
    <?php endif; ?>

    <div class="w3-display-container w3-hide-small">
        <?php // echo bkw3s_content_image_full_wide(get_the_ID()) ?>
        <?php echo get_the_post_thumbnail( get_the_ID(), 'full' , ['class' => 'w3-image', 'style' => 'width:100%']); ?>
    </div>

    <div id="content" class="w3-container w3-row">
        <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
        <div class="w3-col l8 m10">
            <section id="content-section">
